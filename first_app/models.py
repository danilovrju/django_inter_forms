from django.db import models


class FirstAppModel(models.Model):
    some_field = models.CharField(max_length=30)

    def __str__(self):
        return self.some_field


class FirstAppModel2(models.Model):
    some_field = models.CharField(max_length=30)
    field_from_firstapp = models.ForeignKey(
        FirstAppModel,
        on_delete=models.SET_NULL,
        null=True,
    )

    def __str__(self):
        return self.some_field
