from django.contrib import admin
from .models import FirstAppModel, FirstAppModel2

# Register your models here.
@admin.register(FirstAppModel)
class FirstAppModelAdmin(admin.ModelAdmin):
    fields = ["some_field"]


@admin.register(FirstAppModel2)
class FirstAppModel2Admin(admin.ModelAdmin):
    fields = ["some_field", "field_from_firstapp"]