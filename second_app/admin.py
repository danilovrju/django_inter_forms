from django.contrib import admin
from .models import SecondAppModel

# Register your models here.
@admin.register(SecondAppModel)
class SecondAppModelAdmin(admin.ModelAdmin):
    fields = ["some_field", "field_from_firstapp"]