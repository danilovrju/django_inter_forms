from django.db import models
from first_app.models import FirstAppModel


class SecondAppModel(models.Model):
    some_field = models.CharField(max_length=30)
    field_from_firstapp = models.ForeignKey(
        FirstAppModel,
        on_delete=models.SET_NULL,
        null=True,
    )
